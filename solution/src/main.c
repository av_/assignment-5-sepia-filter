#include <time.h>
#include "bmp.h"
#include "error_handling.h"
#include "file.h"
#include "image.h"
#include "sepia.h"

int main( int argc, char** argv ) {
    if (argc != 4) {
        return print_error_and_return_status(WRONG_NUMBER_OF_ARGS);
    }

    char *input_filepath = argv[1];
    char *output_c_filepath = argv[2];
    char *output_asm_filepath = argv[3];

    FILE *input_file, *c_output_file, *asm_output_file;

    if (open_file(&input_file, input_filepath, "rb") != OPEN_OK) {
        return print_error_and_return_status(WRONG_INPUT_PATH);
    }

    if (open_file(&c_output_file, output_c_filepath, "wb") != OPEN_OK) {
        if (close_file(input_file) != CLOSE_OK) {
            print_error_and_return_status(FILE_CLOSE_ERROR);
        }
        return print_error_and_return_status(WRONG_C_OUTPUT_PATH);
    }

    if (open_file(&asm_output_file, output_asm_filepath, "wb") != OPEN_OK) {
        if (close_file(input_file) != CLOSE_OK) {
            print_error_and_return_status(FILE_CLOSE_ERROR);
        }
        if (close_file(c_output_file) != CLOSE_OK) {
            print_error_and_return_status(FILE_CLOSE_ERROR);
        }
        return print_error_and_return_status(WRONG_C_OUTPUT_PATH);
    }

    struct image img = {0};
    enum read_status read_bmp_status = from_bmp(input_file, &img);

    if (close_file(input_file) != CLOSE_OK) {
        return print_error_and_return_status(FILE_CLOSE_ERROR);
    }

    if (read_bmp_status != READ_OK) {
        destroy_image(&img);
        return print_error_with_status(READ_IMAGE_ERROR, read_bmp_status);
    }

    clock_t start, end;

    start = clock();
    struct image sepia_image_c = sepia_c(&img);
    end = clock();

    enum write_status write_bmp_status_c = to_bmp(c_output_file, &sepia_image_c);

    destroy_image(&sepia_image_c);

    printf("[C implementation] time of execution: %f milliseconds\n",
           ((double)(end - start))/CLOCKS_PER_SEC * 1000);

    if (close_file(c_output_file) != CLOSE_OK) {
        return print_error_and_return_status(FILE_CLOSE_ERROR);
    }

    start = clock();
    struct image sepia_image_asm = sepia_asm(&img);
    end = clock();
    enum write_status write_bmp_status_asm = to_bmp(asm_output_file, &sepia_image_asm);

    destroy_image(&img);

    destroy_image(&sepia_image_asm);

    printf("[ASM implementation] time of execution: %f milliseconds\n\n",
           ((double)(end - start))/CLOCKS_PER_SEC * 1000);

    if (close_file(asm_output_file) != CLOSE_OK) {
        return print_error_and_return_status(FILE_CLOSE_ERROR);
    }

    if (write_bmp_status_c != WRITE_OK || write_bmp_status_asm != WRITE_OK) {
        return print_error_with_status(
                WRITE_IMAGE_ERROR,
                write_bmp_status_c != WRITE_OK ? (int) write_bmp_status_c : (int) write_bmp_status_asm
                );
    }

    return OK;
}
