#include "image.h"
#include "pixel.h"
#include "sepia.h"

#include <stddef.h>

static inline uint8_t to_valid_color_value(uint16_t val) {
    return val > 255 ? 255 : val;
}

static inline struct pixel sepia_pixel(struct pixel* pixel) {
    uint16_t red = (uint16_t) (0.393 * pixel->r + 0.769 * pixel->g + 0.189 * pixel->b);
    uint16_t green = (uint16_t) (0.349 * pixel->r + 0.686 * pixel->g + 0.168 * pixel->b);
    uint16_t blue = (uint16_t) (0.272 * pixel->r + 0.534 * pixel->g + 0.131 * pixel->b);

    return (struct pixel) {
        .r = to_valid_color_value(red),
        .g = to_valid_color_value(green),
        .b = to_valid_color_value(blue)
    };
}

struct image sepia_c(struct image* image) {
    struct image sepia_image = build_image(image->width, image->height);

    for (size_t i = 0; i < image->height; i++) {
        for (size_t j = 0; j < image->width; j++) {
            set_pixel(&sepia_image, j, i, sepia_pixel(get_pixel(image, j, i)));
        }
    }

    return sepia_image;
}

struct image sepia_asm(struct image* image) {
    struct image sepia_image = build_image(image->width, image->height);

    size_t image_size = image->width * image->height;

    for (size_t i = 0; i < image_size; i += 4) {
        sepia_handle_pixels_asm(image->data + i, sepia_image.data + i);
    }

    for (size_t i = (image_size / 4) * 4 ; i < image_size; i++) {
        sepia_image.data[i] = sepia_pixel(image->data + i);
    }

    return sepia_image;
}
