; Percentages for the new red value

%define r_r 0.393
%define r_g 0.349
%define r_b 0.272

; Percentages for the new green value

%define g_r 0.769
%define g_g 0.686
%define g_b 0.534

; Percentages for the new blue value

%define b_r 0.189
%define b_g 0.168
%define b_b 0.131

; In order to get new red, blue, and green values, you need to do the following:
;
;       (R R R) x (r_b r_g r_r)
;     + (G G G) x (g_b g_g g_r)
;     + (B B B) x (b_b b_g b_r)
;       -------
;       (newB newG newR)
;
; However, since we are aiming to improve time performance here, and xmm regs have space for four floats,
; in 3 steps we can calculate values for 4 pixels:
;       1. newB1 newG1 newR1 newB2
;       2. newG2 newR2 newB3 newG3
;       3. newR3 newB4 newG4 newR4
;
; It is assumed that this function is only dealing with blocks of 4 pixels, since in other scenarios it is not
; substantially faster than the C realization.

%macro define_shuffle 1
    %if %1==1
        ; P1 P2 _ _ -> P1 P1 P1 P2
        %define SHUFFLE_PATTERN 0b00000001
    %elif %1==2
        ; P1 P2 _ _ -> P1 P1 P2 P2
        %define SHUFFLE_PATTERN 0b00000101
    %elif %1==3
        ; P1 P2 _ _ -> P1 P2 P2 P2
        %define SHUFFLE_PATTERN 0b00010101
    %else
        %error "invalid operand for define_shuffle macro (must be 1, 2 or 3)"
    %endif
%endmacro

; load next 4 pixels in xmm0, xmm1, xmm2
%macro load_values 1
    pxor xmm0, xmm0
    pxor xmm1, xmm1
    pxor xmm2, xmm2

    define_shuffle %1

    ; blues
    pinsrb xmm2, [rdi], 0 ; pixel 1
    pinsrb xmm2, [rdi + 3], 4 ; pixel 2
    pshufd xmm2, xmm2, SHUFFLE_PATTERN

    ; greens
    pinsrb xmm1, [rdi + 1], 0 ; pixel 1
    pinsrb xmm1, [rdi + 4], 4 ; pixel 2
    pshufd xmm1, xmm1, SHUFFLE_PATTERN

    ; reds
    pinsrb xmm0, [rdi + 2], 0 ; pixel 1
    pinsrb xmm0, [rdi + 5], 4 ; pixel 2
    pshufd xmm0, xmm0, SHUFFLE_PATTERN
%endmacro

%macro get_new_values 0
    ; convert pixel values to floats
    cvtdq2ps xmm0, xmm0
    cvtdq2ps xmm1, xmm1
    cvtdq2ps xmm2, xmm2

    ; multiply vectors
    mulps xmm0, xmm3 ; reds
    mulps xmm1, xmm4 ; greens
    mulps xmm2, xmm5 ; blues

    ; add
    addps xmm0, xmm1
    addps xmm0, xmm2

    ; convert to unsigned ints
    cvtps2dq xmm0, xmm0

    ; if any of the values are greater than 255 set to 255
    pminud xmm0, xmm6
%endmacro

; Multiplication vectors:
section .rodata

    align 16

    step1_reds: dd r_b, r_g, r_r, r_b
    step1_greens: dd g_b, g_g, g_r, g_b
    step1_blues: dd b_b, b_g, b_r, b_b

    step2_reds: dd r_g, r_r, r_b, r_g
    step2_greens: dd g_g, g_r, g_b, g_g
    step2_blues: dd b_g, b_r, b_b, b_g

    step3_reds: dd r_r, r_b, r_g, r_r
    step3_greens: dd g_r, g_b, g_g, g_r
    step3_blues: dd b_r, b_b, b_g, b_r

    max_values: dd 255, 255, 255, 255

section .text

global sepia_handle_pixels_asm
sepia_handle_pixels_asm:
    mov rax, max_values
    movdqa xmm6, [rax] ; using rax to avoid relocation issues
    .step1:
        load_values 1
        mov rax, step1_reds
        movaps xmm3, [rax]
        mov rax, step1_greens
        movaps xmm4, [rax]
        mov rax, step1_blues
        movaps xmm5, [rax]
        get_new_values
        movdqa xmm7, xmm0 ; saving the result
        add rdi, 3
    .step2:
        load_values 2
        mov rax, step2_reds
        movaps xmm3, [rax]
        mov rax, step2_greens
        movaps xmm4, [rax]
        mov rax, step2_blues
        movaps xmm5, [rax]
        get_new_values
        movdqa xmm8, xmm0 ; saving the result
        add rdi, 3
    .step3:
        load_values 3
        mov rax, step3_reds
        movaps xmm3, [rax]
        mov rax, step3_greens
        movaps xmm4, [rax]
        mov rax, step3_blues
        movaps xmm5, [rax]
        get_new_values
        movdqa xmm9, xmm0 ; saving the result
    .set_pixels:
        ; saving the results in the end and not after each step because rdi and rsi could point to the same address
        pextrb [rsi], xmm7, 0
        pextrb [rsi + 1], xmm7, 4
        pextrb [rsi + 2], xmm7, 8
        pextrb [rsi + 3], xmm7, 12
        pextrb [rsi + 4], xmm8, 0
        pextrb [rsi + 5], xmm8, 4
        pextrb [rsi + 6], xmm8, 8
        pextrb [rsi + 7], xmm8, 12
        pextrb [rsi + 8], xmm9, 0
        pextrb [rsi + 9], xmm9, 4
        pextrb [rsi + 10], xmm9, 8
        pextrb [rsi + 11], xmm9, 12
        ret
