#include "file.h"
#include <stdio.h>

enum open_file_status open_file(FILE** file, const char* const file_path, const char* const mode) {
    *file = fopen(file_path, mode);
    if (*file == NULL) {
        return OPEN_ERROR;
    }
    return OPEN_OK;
}

enum close_file_status close_file(FILE * file) {
    if (fclose(file) != 0) {
        return CLOSE_ERROR;
    }
    return CLOSE_OK;
}
