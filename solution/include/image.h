#ifndef ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H

#include "pixel.h"
#include <inttypes.h>

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image build_image(uint64_t width, uint64_t height);

void destroy_image(struct image* img);

struct pixel* get_pixel(struct image* image, uint64_t column, uint64_t row);

void set_pixel(struct image* img, uint64_t column, uint64_t row, struct pixel value);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
