#ifndef ASSIGNMENT_5_SEPIA_SEPIA
#define ASSIGNMENT_5_SEPIA_SEPIA

#include "image.h"

void sepia_handle_pixels_asm(struct pixel* src_data, struct pixel* dst_data);

struct image sepia_c(struct image* image);

struct image sepia_asm(struct image* image);

#endif //ASSIGNMENT_5_SEPIA_SEPIA
