make
echo '--- [TEST 1] ---' > test_results.txt
./solution/out/sepia tests/1.bmp tests/1c.bmp tests/1asm.bmp >> test_results.txt
echo '--- [TEST 2] ---' >> test_results.txt
./solution/out/sepia tests/2.bmp tests/2c.bmp tests/2asm.bmp >> test_results.txt
echo '--- [TEST 3]- --' >> test_results.txt
./solution/out/sepia tests/3.bmp tests/3c.bmp tests/3asm.bmp >> test_results.txt